<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<%@page import="jpautil.*"%>
<%@page import="javax.persistence.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<c:set scope="session" var="currentPage" value="/book.jsp" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title><fmt:message key="view.title.shop"/></title>
</head>
<body>
  <div id="global">
    <div id="top"><jsp:include page="top.jsp" /></div>
    <div id="content">
      <div id="left"><jsp:include page="left.jsp" /></div>
      <div id="center">
    		<p><fmt:message key="view.cart.title"/> - ${books.title}</p>
    		<br />
    		<p><fmt:message key="view.cart.price"/> - ${books.price} <fmt:message key="view.book.euro"/></p>
    		<br />
    		<img src="bookPhoto.do?bookId=${books.id}" />
    		
    		<form action="book.do">
   				<input type="hidden" name="bookId" value="${books.id}" />
   				<input type="hidden" name="action" value="addOne" />
   				<input type="submit" name="dummy" value="<fmt:message key="view.cart.add"/>" />
			</form>
      </div>
      <div id="right"><jsp:include page="right.jsp" /></div>
    </div>
  </div>
</body>
</html>
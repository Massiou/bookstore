<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<%@page import="jpautil.*"%>
<%@page import="javax.persistence.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
    <title><fmt:message key="view.title.category"/></title>
  </head>
  <body> 
<%--
Le JSP classique avec des scriptlets Java
--%>
    <%
      EntityManager em = JpaFilter.getEntityManager();
      List<Category> categories = (List<Category>) em.createQuery("from model.Category").getResultList();
      pageContext.setAttribute("categories", categories);
    %>
    <table>
      <%
        for (Category category : categories) {
      %>
      		<tr>
        		<td><a href="category.jsp?categoryId=<%=category.getId()%>"><%=category.getTitle()%></a></td>
      		</tr>
      <%
        }
      %>
    </table>

   <%
      pageContext.setAttribute("categories", categories);
    %>
      <table>
      <c:forEach var="category" items="${categories}">
      <tr>
        <td><a href="category.jsp?categoryId=${category.id}">${category.title}</a></td>
      </tr>
      </c:forEach>
    </table>
  </body>
</html>
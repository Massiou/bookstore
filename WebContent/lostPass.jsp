<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set scope="session" var="currentPage" value="/accueil.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title>Bookstore</title>
</head>
<body>
  <div id="global">
    <div id="top"><jsp:include page="top.jsp" /></div>
    <div id="content">
      <div id="left"><jsp:include page="left.jsp" /></div>
      <div id="center">
        <p>Mot de passe oublié</p>
        
        <form action="lostPass.do" method="get">
		   <p>Veuillez entrer votre adresse e-mail pour que notre équipe puisse votre renvoyer votre mot de passe</p>
		   E-mail : <input type="text" name="lostPassMailValue" value="" /> <br/>
		   <input type="hidden" name="action" value="lostPass" />
		   <input type="submit" name="dummy" value="Envoyer" />
		</form>           
      </div>
      <div id="right"><jsp:include page="right.jsp" /></div>
    </div>
  </div>
</body>
</html>
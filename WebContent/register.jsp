<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title><fmt:message key="view.title.register"/></title>
</head>
<body>
	<div id="global">
    	<div id="top"><jsp:include page="top.jsp" /></div>
    	<div id="content">
      			<div id="center">
      			
    				<c:if test="${not empty registerMessage}"><fmt:message key="${registerMessage}"/></c:if>
    				
    				<form action="register.do">
    					<p>Informations de connexion</p>
    					<fieldset id="infoConnexion">
    						<fmt:message key="register.firstName"/><input type="text" name="firstName" /><br />
    						<fmt:message key="register.name"/><input type="text" name="name" /><br />
    						<fmt:message key="register.login"/><input type="text" name="login" /><br />
   							<fmt:message key="register.mail"/><input type="text" name="email" /><br />
   							<fmt:message key="register.mdp"/><input type="password" name="password" /><br />
   							<fmt:message key="register.confirm"/><input type="password" name="passwordConfirmed" /><br />
    					</fieldset>
   						<p>Informations de livraison</p>
   						<fieldset id="infoLivraison">
   							<fmt:message key="register.adresse"/><input type="text" name="adresse" /><br />
   							<fmt:message key="register.adresseLivraison"/><input type="text" name="adresseLivraison" /><br />
   							<fmt:message key="register.numTel"/><input type="text" name="numTel" /><br />
   						</fieldset>
   						<input type="hidden" name="action" value="register" />
   						<p>les champs suivis d'une * sont obligatoires</p>
   						<input type="submit" name="dummy" value="<fmt:message key="register.button.confirm"/>" />
					</form>
					
      			</div>
    	</div>
  	</div>
</body>
</html>
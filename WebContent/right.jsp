<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:useBean scope="session" id="panier" class="model.Order"></jsp:useBean>
 
<div class="boite">
 <p><fmt:message key="view.cart.yourCart"/></p>
 <a href="panier.jsp"><fmt:message key="view.cart.content"/></a>
 ${panier.taillePanier} <fmt:message key="view.cart.articles"/>
 
</div>
<div class="boite"> 

<!-- Conditionement de la vue, si user non connecté : on lui affiche le formulaire de connexion -->
<c:choose>
	<c:when test="${user.login != null}">
		<p><fmt:message key="login.loggedAs"/></p>
		${user.login}
		<form action="login.do">
   				<input type="hidden" name="action" value="disco" />
   				<input type="submit" name="dummy" value="Deconnexion" />
 		</form>
	</c:when>
	
	<c:otherwise>
		<p><fmt:message key="login.connect"/></p>
		<form action="login.do">
   				<fmt:message key="login.login"/><input type="text" name="login" /><br />
   				<fmt:message key="login.mdp"/><input type="password" name="password" /><br />
   				<input type="hidden" name="action" value="connect" />
   				<input type="submit" name="dummy" value="<fmt:message key="login.connect"/>" />
 		</form>
 		<a href="register.jsp"><fmt:message key="register.navigate"/></a>
 		<a href="lostPass.jsp"><fmt:message key="login.lost"/></a>
	</c:otherwise>
</c:choose>

<c:if test="${not empty loginMessage}"><fmt:message key="${loginMessage}"/></c:if>

</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<c:set scope="session" var="currentPage" value="/accueil.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title><fmt:message key="view.title.home"/></title>
</head>
<body>
  <div id="global">
    <div id="top"><jsp:include page="top.jsp" /></div>
    <div id="content">
      <div id="left"><jsp:include page="left.jsp" /></div>
      <div id="center">
        <fmt:message key="view.home"/><br />
        <fmt:message key="view.bestSales"/>
      </div>
      <div id="right"><jsp:include page="right.jsp" /></div>
    </div>
  </div>
</body>
</html>
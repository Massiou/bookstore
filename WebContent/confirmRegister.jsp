<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title>Confirmation d'inscription</title>
</head>
<body>
	<div id="global">
    	<div id="top"><jsp:include page="top.jsp" /></div>
    		<div id="content">
    		<fmt:message key="register.confirmation"/>
    		<fmt:message key="register.mailSent"/>
    		<fmt:message key="register.redirection"/>
    		</div>
    </div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<%@page import="jpautil.*"%>
<%@page import="javax.persistence.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<%
		EntityManager em = JpaFilter.getEntityManager();
    	List<Category> categories = (List<Category>) em.createQuery("from model.Category").getResultList();
    %>
      
<div class="boite">
	<%
      pageContext.setAttribute("categories", categories);
    %>
      <table>
      <c:forEach var="category" items="${categories}">
      <tr>
        <td><a href="category.jsp?categoryId=${category.id}">${category.title}</a></td>
      </tr>
      </c:forEach>
    </table>
</div>
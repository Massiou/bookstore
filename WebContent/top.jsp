<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page import="model.*"%>

<%
	String languageFr = request.getParameter("Fr");
	
	if(languageFr != null && languageFr.equals("T")){
%>
		<fmt:setLocale value="fr_FR" scope="session"/>
<%
	}else if(languageFr != null && languageFr.equals("F")){
%>
		<fmt:setLocale value="en_US" scope="session"/>
<%
	}
%>

<div class="boite" align="right">
	<a href="accueil.jsp?Fr=T"><img src="./resources/icons/flags/fr_FR.png"/></a>
	<a href="accueil.jsp?Fr=F"><img src="./resources/icons/flags/en_US.png"/></a>
</div>
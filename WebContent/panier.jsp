<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<%@page import="jpautil.*"%>
<%@page import="javax.persistence.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<c:set scope="session" var="currentPage" value="/panier.jsp" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title><fmt:message key="view.title.cart"/></title>
</head>
<body>
  <div id="global">
    <div id="top"><jsp:include page="top.jsp" /></div>
    <div id="content">
      <div id="left"><jsp:include page="left.jsp" /></div>
      <div id="center">
      		<p><fmt:message key="view.cart.yourCart"/></p>
    		<table>
    		<tr>
      		 	<th><fmt:message key="view.cart.yourCart"/></th>
      		 	<th><fmt:message key="view.cart.quantity"/></th>
      		 	<th><fmt:message key="view.cart.minus"/></th>
      		 	<th><fmt:message key="view.cart.plus"/></th>
      		 	<th><fmt:message key="view.cart.price"/></th>
      		 </tr>
      		 <c:forEach var="item" items="${panier.listItems}">
      		 <tr>
      		 	<td>${item.book.title}</td>
        		<td>${item.quantity}</td>
        		<td>
        			<form action="panier.do">
        				<input type="hidden" name="bookId" value="${item.book.id}" />
   						<input type="hidden" name="action" value="decrease" />
   						<input type="submit" name="dummy" value="-" />
 					</form>
 				</td>
        		<td>
        			<form action="panier.do">
        				<input type="hidden" name="bookId" value="${item.book.id}" />
   						<input type="hidden" name="action" value="increase" />
   						<input type="submit" name="dummy" value="+" />
 					</form>
 				</td>
        		<td>${item.totalPriceOrderItem}</td>
      		 </tr>
      		 </c:forEach>
    		</table>
    		<p><fmt:message key="view.cart.total"/> ${panier.prixTotalPanier}</p>
      </div>
      <div id="right"><jsp:include page="right.jsp" /></div>
    </div>
  </div>
</body>
</html>
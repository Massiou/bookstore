<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<%@page import="jpautil.*"%>
<%@page import="javax.persistence.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<c:set scope="session" var="currentPage" value="/category.jsp" />
   		<%
      			
				EntityManager em = JpaFilter.getEntityManager();
      		
      			//récupération de l'ID de la catégory passée en paramètre de la requête
      			//conversion du param String -> Long
      			String categoryID = request.getParameter("categoryId");
      			Long longCategoryID = Long.parseLong(categoryID);
				//recherche dans l'entité Category via l'ID category
      			Category category = em.find(Category.class, longCategoryID);
				//obtention de la liste des livres associés à la category spécifiée
    			List<Book> books = category.getBooks();
    			
    		    pageContext.setAttribute("books", books);
      		    pageContext.setAttribute("category", category);
      		  
    		%>
  <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<title><fmt:message key="view.title.category"/></title>
</head>
<body>
  <div id="global">
    <div id="top"><jsp:include page="top.jsp" /></div>
    <div id="content">
      <div id="left"><jsp:include page="left.jsp" /></div>
      <div id="center">
     		${category.title}
    		 <table>
      		 <c:forEach var="book" items="${books}">
      		 <tr>
        		<td><a href="book.do?bookId=${book.id}" action=display>${book.title}</a></td>
      		 </tr>
      		 </c:forEach>
      </div>
      <div id="right"><jsp:include page="right.jsp" /></div>
    </div>
  </div>
</body>
</html>
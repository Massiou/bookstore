package servlet;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jpautil.JpaFilter;
import model.Book;

/**
 * Servlet implementation class PhotoServlet
 */
@WebServlet({ "/PhotoServlet", "/bookPhoto.do" })
public class PhotoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhotoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EntityManager em = JpaFilter.getEntityManager();
		
		//r�cup�ration de l'ID du book pass�e en param�tre de la requ�te
		String bookID = request.getParameter("bookId");
		//conversion du param String -> Long
		Long longBookID = Long.parseLong(bookID);
		//on retrouve le Book associ� � l'ID r�cup�r�
		Book book = em.find(Book.class, longBookID);
		
		//on set le type de r�ponse qui sera retourn�e par la servlet
		response.setContentType("image/jpeg");
		//on lie l'output stream avec la r�ponse de la servlet
		OutputStream outPS = new BufferedOutputStream(response.getOutputStream());
		//on �crit dans l'output stream le contenu d�sir�
		outPS.write(book.getPhoto());
		//on ferme l'outputStream une fois utilis�e
		outPS.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

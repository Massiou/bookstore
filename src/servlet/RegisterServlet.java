package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Client;
import jpautil.JpaFilter;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet({ "/RegisterServlet", "/register.do" })
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
    }
        
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*
		 * Récupération de l'entity manager
		 */
		EntityManager hsession = JpaFilter.getEntityManager();
		
		Map<String, String> properties = new HashMap<String, String>();
	    properties.put("hibernate.hbm2ddl.auto", "create-drop");
	    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("BookStore", properties);
	    EntityManager entityManager = entityManagerFactory.createEntityManager();
	    entityManager.getTransaction().begin();
		
		/*
		 * Obtention des champs "Informations de connexion"
		 */
		
		String firstName = request.getParameter("firstName");
		String name = request.getParameter("name");
		String login = request.getParameter("login");
		String mail = request.getParameter("email");
		String password = request.getParameter("password");
		String passwordConfirmed = request.getParameter("passwordConfirmed");
		
		/*
		 * Obtention des champs "Informations de livraison"
		 */
		String adresse = request.getParameter("adresse");
		String adresseLivraison = request.getParameter("adresseLivraison");
		String numTel = request.getParameter("numTel");
		
		
		
		/*
		 * Récupération de la session HTTP pour enregistrement du client
		 */
		HttpSession session = request.getSession();
		
		/*
		 * Traitement des informations de connexion pour enregistrement
		 */
		if(request.getParameter("action") != null && request.getParameter("action").equals("register")){
			/*
			 * Vérification de la présence des informations de connexion obligatoires
			 */
			
			
			if((login != null && !login.isEmpty()) && (mail != null && !mail.isEmpty()) && (password != null && !password.isEmpty()) &&
					(passwordConfirmed != null && !passwordConfirmed.isEmpty())){
				
				/*
				 * Vérification de la non-existence du login du nouvel utilisateur
				 */
				try{
					Client client = (Client) hsession.createQuery("from model.Client where login = :login").setParameter("login", login).getSingleResult();
					if(client.getLogin().equals(login)){
						request.setAttribute("registerMessage", "register.alreadyExists");
						getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
					}
				
				}catch(NoResultException e){
					/*
					 * Aucune information remontée de base, le login n'existe pas
					 */
				}
				
				/*
				 * Vérification du mot de passe de confirmation
				 */
				if(password.equals(passwordConfirmed)){
					Client newClient = new Client();
					/*
					 * Enregistrement des infos de connexion
					 */
					
					newClient.setFirstName(firstName);
					newClient.setName(name);
					newClient.setLogin(login);
					newClient.setPassword(password);
					newClient.setMail(mail);
					
					/*
					 * Enregistrement des infos de livraison
					 */
					
					newClient.setAdresse(adresse);
					if(adresseLivraison != null && !adresseLivraison.isEmpty()){
						newClient.setAdresseLivraison(adresseLivraison);
					}else{
						newClient.setAdresseLivraison(adresse);
					}
					newClient.setNumTel(numTel);
					/*
					 * Commit BDD des données settées dans l'objet user
					 */
					
					entityManager.persist(newClient);
					entityManager.getTransaction().commit();
					getServletContext().getRequestDispatcher("/confirmRegister.jsp").forward(request, response);
					
					
				}else{
					request.setAttribute("registerMessage", "register.field.differentPswd");
					getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
				}
				
			}else{
				request.setAttribute("registerMessage", "register.field.required");
				getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
			}
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

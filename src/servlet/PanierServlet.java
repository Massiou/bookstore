package servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Book;
import model.Order;

import jpautil.JpaFilter;

/**
 * Servlet implementation class PanierServlet
 */
@WebServlet({ "/PanierServlet", "/panier.do" })
public class PanierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PanierServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EntityManager em = JpaFilter.getEntityManager();
		
		/*r�cup�ration de l'ID du book pass�e en param�tre de la requ�te
		conversion du param String -> Long*/
		String bookID = request.getParameter("bookId");
		Long longBookID = Long.parseLong(bookID);
		Book book = em.find(Book.class, longBookID);
		
		/*
		 * r�cup�ration du panier dans la session HTTP
		 */
		Order panier = (Order)request.getSession().getAttribute("panier");
		/*
		 * Param action = increase : 
		 * on d�clenche l'action addBook
		 * - incr�mente de 1 la quantit�
		 * - incr�mente de 1 la taille totale du panier
		 * - MAJ le prix total pour cet item
		 * - MAJ le prix total du panier
		 */
		
		if(request.getParameter("action") != null && request.getParameter("action").equals("increase")){
			panier.addBook(book);
		}
		/*
		 * Param action = decrease : 
		 * on d�clenche l'action removeBook
		 * - g�re la quantit� (si quantit� = 0 : on enl�ve le livre de la liste)
		 * - incr�mente de 1 la taille totale du panier
		 * - MAJ le prix total pour cet item
		 * - MAJ le prix total du panier
		 */
		if(request.getParameter("action") != null && request.getParameter("action").equals("decrease")){
			panier.removeBook(book);
		}
		
		/*
		 * post-traitement : redirige pour de nouveau afficher le panier
		 */
		getServletContext().getRequestDispatcher("/panier.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

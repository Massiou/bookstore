package servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import jpautil.JpaFilter;

import model.Client;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet({ "/LoginServlet", "/login.do" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * R�cup�ration de l'entity manager
		 */
		EntityManager hsession = JpaFilter.getEntityManager();
		
		/*
		 * Obtention du param�tre de requ�te 'login'
		 */
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		/*
		 * R�cup�ration de la session HTTP pour enregistrement du client
		 */
		HttpSession session = request.getSession();
		
		/*
		 * Action = connect : un utilisateur veut s'identifier
		 * Recherche du login/mdp pr�cis� dans la BDD
		 */
		if(request.getParameter("action") != null && request.getParameter("action").equals("connect")){
			try{
				/*
				 * construction et execution de la requ�te de recherche de l'utilisateur avec les param�tres pass�s
				 */
				Client client = (Client) hsession.createQuery("from model.Client where login = :login and password = :password").setParameter("login", login).setParameter("password", password).getSingleResult();
				/*
				 * Si un utilisateur a �t� trouv�, on l'enregistre dans la session HTTP
				 */
				session.setAttribute("user", client);

				/*
				 * puis on redirige l'utilisateur vers l'accueil
				 */
				getServletContext().getRequestDispatcher("/accueil.jsp").forward(request, response);
				
			}catch(NoResultException e){
				/*
				 * Si login/mdp non-trouv� : on affiche un message en levant une exception
				 */
				request.setAttribute("loginMessage", "login.notFound");
				getServletContext().getRequestDispatcher("/accueil.jsp").forward(request, response);
			}
			
		}
		/*
		 * Action = disco : un utilisateur veut se d�connecter
		 */
		if(request.getParameter("action") != null && request.getParameter("action").equals("disco")){
			/*
			 * Si l'action vaut se deconnecter, on supprime l'attribut user de la session HTTP
			 */
			session.removeAttribute("user");
			/*
			 * on redirige ensuite vers la page d'accueil
			 */
			getServletContext().getRequestDispatcher("/accueil.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package servlet;

import javax.mail.* ;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Properties;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jpautil.JpaFilter;
import model.Client;
import model.SendMailTLS;

/**
 * Servlet implementation class LostPassServlet
 */
@WebServlet("/lostPass.do")
public class LostPassServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LostPassServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("LostPassServlet");
		
		Client client = null ;
		String pass = null ;
		EntityManager em = JpaFilter.getEntityManager();
		
		//On extrait les param�tres, donc l'adresse mail de l'utilisateur
		String paramMail = request.getParameter("lostPassMailValue") ; System.out.println(paramMail);
		
		if(paramMail.equals("")){
			request.setAttribute("LostPassMessage", "field.required") ;
			request.getRequestDispatcher("/lostPass.jsp").forward(request, response) ;
			return ;
		}else{
			String mail = paramMail ;
			
			try{							
				HttpSession session = request.getSession(true) ;				
		
				//On retrouve le mot de passe associ�
				pass = (String) em.createQuery("select password from model.Client where mail = :mail").setParameter("mail", mail).getSingleResult(); System.out.println(pass);
				
				//Envoi du mot de passe au mail associ�
				String messageText = "bonjour, \n\n" +
									"Voici votre mot de passe : " + pass + "\n\n" +
									"Cordialement" ;
				
				SendMailTLS.sendMail("jeanlouis.duong@gmail.com", "Mot de passe oubli�", messageText) ;
				
				session.setAttribute("lostPassMessage", "lostPass.sendMailSuccess") ;
			}catch(NoResultException e){
				request.setAttribute("lostPassMessage", "lostPass.mailNotFound") ;
			}
		}
		
		request.getRequestDispatcher("/accueil.jsp").forward(request, response) ;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

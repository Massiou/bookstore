package servlet;

import java.io.IOException;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jpautil.JpaFilter;

import model.Book;
import model.Order;
import model.OrderItem;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet({ "/BookServlet", "/book.do" })
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			EntityManager em = JpaFilter.getEntityManager();
		
			/*r�cup�ration de l'ID du book pass�e en param�tre de la requ�te
			conversion du param String -> Long*/
			String bookID = request.getParameter("bookId");
			Long longBookID = Long.parseLong(bookID);
			/*on retrouve le Book associ� � l'ID r�cup�r�*/
			Book book = em.find(Book.class, longBookID);
			
			/*si le param�tre addOne est sett� dans la requ�te...
			 * on retrouve le panier dans le scope de session
			 * et on ajoute l'ouvrage
			 */
			if(request.getParameter("action") != null && request.getParameter("action").equals("addOne")){
					Order panier = (Order)request.getSession().getAttribute("panier");
					panier.addBook(book);
			}
			
			/*on set le book remont� en tant que param�tre de la requ�te*/
			request.setAttribute("books", book);
			/*forward vers la page book.jsp*/
			getServletContext().getRequestDispatcher("/book.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}

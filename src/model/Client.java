package model;

import javax.persistence.Entity;

@Entity
public class Client extends Persistent{
	
	private String login;
	private String password;
	private String name;
	private String firstName;
	private String mail;
	private String adresse;
	private String adresseLivraison;
	private String numTel;
	
	public Client(){
		login = null;
		password = null;
	}
	
	/**
	 * @return le login d'un utilisateur
	 */
	public String getLogin(){
		return login;
	}
	/**
	 * @return le mot de passe d'un utilisateur
	 */
	public String getPassword(){
		return password;
	}
	/**
	 * @return le nom de l'utilisateur
	 */
	public String getName(){
		return name;
	}
	/**
	 * @return le pr�nom de l'utilisateur
	 */
	public String getFirstName(){
		return firstName;
	}
	
	public String getMail(){
		return mail;
	}
	
	public String getAdresse(){
		return mail;
	}
	
	public String getAdresseLivraison(){
		return adresseLivraison;
	}
	
	public String getNumTel(){
		return numTel;
	}
	
	/**
	 * @param alterLogin
	 * set le login d'un utilisateur
	 */
	public void setLogin(String alterLogin){
		login = alterLogin;
	}
	/**
	 * @param alterpassword
	 * set le password d'un utilisateur
	 */
	public void setPassword(String alterPassword){
		password = alterPassword;
	}
	/**
	 * @param alterName
	 * set le nom de l'utilisateur
	 */
	public void setName(String alterName){
		name = alterName;
	}
	/**
	 * @param alterFirstName
	 * set le pr�nom de l'utilisateur
	 */
	public void setFirstName(String alterFirstName){
		firstName = alterFirstName;
	}
	
	public void setMail(String alterMail){
		mail = alterMail;
	}
	
	public void setAdresse(String alterAdresse){
		adresse = alterAdresse;
	}
	
	public void setAdresseLivraison(String alterAdresseLivraison){
		adresseLivraison = alterAdresseLivraison;
	}
	
	public void setNumTel(String alterNumTel){
		numTel = alterNumTel;
	}
	
}

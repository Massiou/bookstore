package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Order extends Persistent{

	/*
	 On doit pouvoir connaitre d'un panier :
	 	- les livres qu'il contient
	 	- le nombre de livres qu'il contient
	 	- le prix total des livres qu'il contient
	 */

	private List<OrderItem> listItems;
	private int taillePanier;
	private float prixTotalPanier;
	
	/**
	 * Un nouveau panier est une nouvelle liste d'items
	 * il ne contient aucun ouvrage
	 * son prix total est de 0
	 */
	public Order(){
		listItems = new ArrayList<OrderItem>();
		taillePanier = 0;
		prixTotalPanier = 10;
	}
	
	/**
	 * @param orderItem
	 * 
	 * ajoute un livre au panier
	 * incr�mente le nombre de livres pr�sents dans le panier
	 * met � jour le prix total du panier
	 */
	public void addBook(Book bookToAdd){
		
		/*on v�rifie si le livre est d�ja pr�sent dans la liste d'orders
		  - si oui, on en augmente juste la quantit� pr�sente dans le panier
		  - et on calcule aussi le prix total pour cet ouvrage
		  - et on met � jour la taille totale du panier
		  - enfin on MAJ le prix total du panier
		*/
		for(OrderItem item : listItems){
			if(item.getBook().equals(bookToAdd)){
				item.increasedQuantity();
				item.setTotalPriceOrderItem(bookToAdd.getPrice());
				increasedSizeOrder();
				calculateTotalPrice();
				return;
			}
		}
		/*si le livre est non pr�sent en tant qu'item, on cr�e un nouvel item*/
		OrderItem orderItemToAdd = new OrderItem(bookToAdd);
		/*on l'ajoute � la liste des items*/
		listItems.add(orderItemToAdd);
		/*met � jour la taille du panier*/
		increasedSizeOrder();
	}
	/**
	 * @param orderItemToRemove
	 * recherche une livre dans le panier
	 * met � jour le prix total du panier
	 * enleve le livre du panier
	 * d�cr�mente le nombre de livres pr�sents dans le panier
	 */
	public void removeBook(Book bookToRemove){
		/*on v�rifie si le livre est d�ja pr�sent dans la liste d'orders
		  - si oui, on en d�cr�mente juste la quantit� pr�sente dans le panier
		  - et on met � jour le prix total pour cet item
		  - et on met � jour la taille totale du panier
		  - enfin on MAJ le prix total du panier
		 */
		for(OrderItem item : listItems){
			if(item.getBook().equals(bookToRemove)){
				item.decreasedQuantity();
				item.setTotalPriceOrderItem(bookToRemove.getPrice());
				/*
				 * On v�rifie que la quantit� de cet ouvrage pr�sent dans le panier est toujours > 0
				 * Si non, alors on enl�ve l'ouvrage de la liste des items
				 */
				if(item.getQuantity() == 0){
					listItems.remove(item);
				}
				decreasedSizeOrder();
				calculateTotalPrice();
				return;
			}
		}
	}
	
	/**
	 * proc�dure ajoutant un livre � la taille totale du panier
	 */
	private void increasedSizeOrder(){
		taillePanier = taillePanier + 1;
	}
	/**
	 * proc�dure enlevant un livre � la taille totale du panier
	 */
	private void decreasedSizeOrder(){
		taillePanier = taillePanier - 1;
	}
	
	/**
	 * @return la taille courante du panier
	 */
	public int getTaillePanier(){
		return taillePanier;
	}
	/**
	 * @return la liste des livres contenus dans le panier
	 */
	public List<OrderItem> getListItems(){
		return listItems;
	}
	/**
	 * @return le prix total du panier
	 * recalcule syst�matiquement le prix total du panier avant de renvoyer le r�sultat
	 */
	public float getPrixTotalPanier(){
		calculateTotalPrice();
		return prixTotalPanier;
	}
	/**
	 * Recalcule le prix total de tous les articles du panier
	 */
	public void calculateTotalPrice(){
		prixTotalPanier = 0;
		/*
		 * prends la liste de tous les livres
		 * somme le prix total de chacun
		 */
		for(OrderItem item : listItems){
			prixTotalPanier = prixTotalPanier + item.getTotalPriceOrderItem();
		}
	}
}

package model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;

@Entity()
public class OrderItem extends Persistent{

	/*
	 Un article doit �tre r�f�renc� par :
		- le livre associ� � l'orderItem
		- la quantit� de livre pour cet item
		- le prix total : prix du livre * quantit�
		- la date d'ajout du livre en tant qu'item
	*/
	
	private Book book;
	private int quantity = 0;
	private float floatTotalPrice = 0;
	private java.util.Date date;
	
	/**
	 * Constructeur
	 * @param bookToAdd
	 * on enregistre le livre associ� � l'orderItem
	 * on enregistre sa date d'insertion
	 * on initie la quantit� pr�sente dans le panier � 1
	 * on initie le prix total au prix de l'ouvrage qu'on vient d'ajouter
	 */
	public OrderItem(Book bookToAdd){
		book = bookToAdd;
		date = new Date();
		increasedQuantity();
		setTotalPriceOrderItem(bookToAdd.getPrice());
	}
	
	/**
	 * @return le livre associ� � l'orderItem courant
	 */
	public Book getBook(){
		return book;
	}
	
	/**
	 * @return le prix du livre � ajouter en tant qu'article
	 */
	public float getTotalPriceOrderItem(){
		return floatTotalPrice;
	}
	/**
	 * @param priceOrderItem
	 * set le prix du livre � ajouter en tant qu'article
	 * effectue : quantit� de l'ouvrage pr�sent * prix de l'ouvrage
	 */
	public void setTotalPriceOrderItem(java.math.BigDecimal priceOrderItem){
		floatTotalPrice = priceOrderItem.floatValue() * quantity;
	}
	/**
	 * @return la date d'ajout du livre en tant qu'article
	 */
	public Date getDateOrderItem(){
		return date;
	}
	/**
	 * @param dateOrderItem
	 * set la date d'ajout du livre en tant qu'article
	 */
	public void setDateOrderItem(Date dateOrderItem){
		date = dateOrderItem;
	}
	
	/**
	 * @return le nombre d'unit�s de l'ouvrage pr�sent dans le panier
	 */
	public int getQuantity(){
		return quantity;
	}
	
	/**
	 * ajoute un exemplaire d'un item
	 */
	public void increasedQuantity(){
		quantity++;
	}
	/**
	 * enl�ve un exemplaire d'un item
	 */
	public void decreasedQuantity(){
		quantity--;
	}
	
}

package test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Author;
import model.Book;
import model.Category;
import model.Client;
import model.Order;

public class CreateBookStore {

  public static void main(String[] args) throws Exception {

    Map<String, String> properties = new HashMap<String, String>();
    properties.put("hibernate.hbm2ddl.auto", "create-drop");
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("BookStore", properties);
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();

    Date now = new Date();
    
    Order order1 = new Order();
    entityManager.persist(order1);

    Category category1 = new Category();
    category1.setTitle("Informatique");

    entityManager.persist(category1);

    Category category2 = new Category();
    category2.setTitle("Littérature");
    entityManager.persist(category2);

    Author author1 = new Author();
    author1.setFirstName("Michel");
    author1.setLastName("Martin");
    entityManager.persist(author1);

    Author author2 = new Author();
    author2.setFirstName("Christian");
    author2.setLastName("Bauer");
    entityManager.persist(author2);

    Author author3 = new Author();
    author3.setFirstName("J-K");
    author3.setLastName("Rowling");
    entityManager.persist(author3);

    Book book = new Book();
    book.setTitle("HTML 5 et CSS 3");
    book.setCategory(category1);

    book.setPrice(new java.math.BigDecimal("10.50"));
    book.setDate(now);
    book.addAuthor(author1);
    File f = new File("WebContent/resources/images/html.jpg");
    byte[] photo = new byte[(int) (f.length())];
    FileInputStream fi = new FileInputStream(f);
    fi.read(photo);
    book.setPhoto(photo);
    entityManager.persist(book);

    book = new Book();
    book.setTitle("Hibernate in action");
    book.setCategory(category1);
    book.setPrice(new java.math.BigDecimal("30.50"));
    book.setDate(now);
    book.addAuthor(author1);
    book.addAuthor(author2);
    f = new File("WebContent/resources/images/hibernate.jpg");
    photo = new byte[(int) (f.length())];
    fi = new FileInputStream(f);
    fi.read(photo);
    book.setPhoto(photo);
    entityManager.persist(book);

    book = new Book();
    book.setTitle("Harry Potter");
    book.setCategory(category2);
    book.setPrice(new java.math.BigDecimal("20.50"));
    book.setDate(now);
    book.addAuthor(author3);
    f = new File("WebContent/resources/images/harry.jpg");
    photo = new byte[(int) (f.length())];
    fi = new FileInputStream(f);
    fi.read(photo);
    book.setPhoto(photo);
    entityManager.persist(book);

    Client user = new Client();
    user.setLogin("galland");
    user.setPassword("dauphine");
    entityManager.persist(user);
    

    entityManager.getTransaction().commit();
  }

}

/*
 * DumpBookStore.java
 */

package test;

import org.hibernate.*;
import org.hibernate.cfg.*;


import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.*;

public class DumpBookStore {
  public static void main(String[] args) throws Exception {
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("BookStore");
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();

     List<Category> categories = entityManager.createQuery("from model.Category").getResultList();
    for (Category category : categories) {
      System.out.println(category.getTitle());
      for(Book book : category.getBooks()) {
        System.out.println(book.getTitle());
        System.out.println(book.getPrice());
        System.out.println(book.getDate());
        for(Author author : book.getAuthors()) {
          System.out.println(author.getLastName());
        }
      }
    }
    entityManager.getTransaction().commit();
 }

}

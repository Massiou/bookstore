/**
 * 
 */
package taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author dga
 *
 */
public class MessagesTag extends SimpleTagSupport{
  private String styleClass;
 

  public String getStyleClass() {
    return styleClass;
  }

  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }
  
  public void doTag() throws JspException {
    PageContext pageContext = (PageContext) getJspContext();
    JspWriter out = pageContext.getOut();
    Messages messages = (Messages)pageContext.getAttribute("messages", PageContext.REQUEST_SCOPE);
    try {
      if(messages != null){
      for(String message: messages.getMessagesList()){
        out.print(message);
        out.print("<br/>");
      }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

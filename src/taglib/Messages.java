/**
 * 
 */
package taglib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dga
 *
 */
public class Messages {
  private Map<String, String> messagesMap = new HashMap<String, String>();
  private List<String> messagesList = new ArrayList<String>();
  
  
  public Map<String, String> getMessagesMap() {
    return messagesMap;
  }


  public List<String> getMessagesList() {
    return messagesList;
  }


  public void add(String message){
    messagesList.add(message);
  }
  public void add(String key, String message){
    messagesMap.put(key, message);
  }
}
